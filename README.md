# Hello World Service

Einfacher Microservice für CI/CD mit Kubernetes.

## Endpoints

### `/greeting`

`http://localhost:3333/greeting?name=Tester`

### Spring Boot Actuator Endpoints

Es werden alle [Spring Boot Actuator Endpoints](https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-endpoints.html)
unterhalb von `http://localhost:3333/actuator` angeboten.
