FROM maven:3.6.2-jdk-8-slim AS build
LABEL autodelete="true"
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package

FROM openjdk:8-jre-alpine
RUN apk add curl && rm -rf /var/cache/apk/*
COPY --from=build /usr/src/app/target/hellosvc-0.*.jar /usr/app/app.jar
VOLUME /tmp
EXPOSE 3333
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/usr/app/app.jar"]
