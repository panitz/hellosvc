FROM openjdk:8-jre-alpine
VOLUME /tmp

RUN apk add curl && rm -rf /var/cache/apk/*

ARG DEPENDENCY=target/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app

EXPOSE 3333
HEALTHCHECK CMD curl -f http://localhost:3333/actuator/health || exit 1;
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-cp", "app:app/lib/*", "com.example.hellosvc.HellosvcApplication"]
