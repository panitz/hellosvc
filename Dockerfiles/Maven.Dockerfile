FROM maven:3.6.1-jdk-8-alpine
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN apk add curl && rm -rf /var/cache/apk/* && mvn -f /usr/src/app/pom.xml clean package

VOLUME /tmp
EXPOSE 3333
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/usr/src/app/target/hellosvc-0.0.1-SNAPSHOT.jar"]
