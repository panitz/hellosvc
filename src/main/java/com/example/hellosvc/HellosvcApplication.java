package com.example.hellosvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HellosvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(HellosvcApplication.class, args);
	}

}
