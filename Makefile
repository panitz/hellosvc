build:
	mvn package
	docker build --rm -t hellosvc -f Dockerfiles/Hybrid.Dockerfile .

build-maven:
	docker build --rm -t hellosvc-maven -f Dockerfiles/Maven.Dockerfile .

build-maven-multistage:
	docker build --rm --force-rm -t hellosvc-maven-multistage -f Dockerfiles/Maven-Multistage.Dockerfile .
	scripts/removeDanglingImages.sh
